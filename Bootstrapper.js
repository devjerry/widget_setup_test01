glbLive = {};

glbLive.Bootstrapper = function() {

	var public = {}
	public.addIframe = addIframe

	function addIframe(outerConfigName, innerConfigName) {
		var vv = {}
		//	Localize config objects
		{
			vv.outerConfig = window[outerConfigName]
			vv.innerConfig = window[innerConfigName]
		}
		//	Ref parent elm.
		{
			vv.parentEl = document.getElementById(vv.outerConfig.iframeId+"Wrap")
		}
		//	Create child (naked) iframe.
		{
			vv.iframe = document.createElement('iframe')
			vv.iframe.setAttribute('id', vv.outerConfig.iframeId)
			vv.iframe.setAttribute('frameborder', '0')
			vv.iframe.setAttribute('scrolling', 'no')
			vv.iframe.setAttribute('src', 'about:blank')
			vv.iframe.setAttribute('data-outer_config_name', outerConfigName)
			vv.iframe.setAttribute('data-inner_config_name', innerConfigName)
		}
		//	Append iframe to parent.
		{
			vv.parentEl.appendChild(vv.iframe)
		}
		//	Inject content into iframe.
		{
			vv.iframeDoc = vv.iframe.contentWindow.document
			vv.iframeCoreHTML = generateCoreHTML(vv.innerConfig, vv.iframe)
			vv.iframeDoc.write(vv.iframeCoreHTML)
			vv.iframeDoc.close()
		}
	}


	function generateCoreHTML(innerConfig) {
		//	Define template.
		{
			var theCoreHTML ='<!DOCTYPE html>'
				+'[-htmlTag-]'
				+'	<head>'
				+'		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'
				+'		<title>[-headTitle-]</title>'
				+'		<scr'+'ipt>'
				+'			console.log( window.frameElement )'
				+'		</scr'+'ipt>'
				+'		[-headCssTagsHere-]'
				+'		[-headJsTagsHere-]'
				+'	</head>'
				+'	[-bodyTag-]'
				+'		<div>'		
				+'			From Bootstrapper.js --top'
				+'		</div>'	
				+'		[-bodyCssTagsHere-]'
				+'		[-bodyJsTagsHere-]'
				+'		<div style="height:200px; border-left:solid 1px green">'		
				+'			Mock vertical space'
				+'		</div>'	
				+'		<div>'		
				+'			From Bootstrapper.js -- bottom'
				+'		</div>'	
				+'	</body>'
				+'</html>'
		}
		//	Replace placeholders with css and js blocks (start from bottom).
		{		
			theCoreHTML = theCoreHTML.replace('[-bodyJsTagsHere-]', bgetResourceTagBlock(innerConfig.bodyJsFileLocs, 'js') )
			
			theCoreHTML = theCoreHTML.replace('[-bodyCssTagsHere-]', bgetResourceTagBlock(innerConfig.bodyCssFileLocs, 'css') )
			
			theCoreHTML = theCoreHTML.replace('[-bodyTag-]', innerConfig.bodyTag )
			
			theCoreHTML = theCoreHTML.replace('[-headJsTagsHere-]', bgetResourceTagBlock(innerConfig.headJsFileLocs, 'js') )
			
			theCoreHTML = theCoreHTML.replace('[-headCssTagsHere-]', bgetResourceTagBlock(innerConfig.headCssFileLocs, 'css') )
			
			theCoreHTML = theCoreHTML.replace('[-headTitle-]', innerConfig.headTitle )
			
			theCoreHTML = theCoreHTML.replace('[-htmlTag-]', innerConfig.htmlTag )
		}

		return theCoreHTML;
	}

	function bgetResourceTagBlock(fileLocs, fileType) {
		vv = {}
		vv.fileLocs = fileLocs
		vv.fileLocsLen = vv.fileLocs.length
		vv.block = ""
		for (vv.idx = 0; vv.idx < vv.fileLocsLen; vv.idx++) {
			vv.itrFileLoc = vv.fileLocs[vv.idx]
			if (fileType === "css") {
				 vv.block = vv.block + "<li"+"nk href='" + vv.itrFileLoc + "' rel='stylesheet' type='text/css'>"
			}
			else if (fileType === "js") {
				 vv.block = vv.block + "<scr"+"ipt src='" + vv.itrFileLoc + "'></scr"+"ipt>"
			}
		}
		return vv.block
	}

	return public
}
