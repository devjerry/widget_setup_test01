function addIframe(parentId, params) {
	var vv = {}
	//	Ref parent elm.
	{
		vv.parentEl = document.getElementById(parentId)
	}
	//	Create child (naked) iframe.
	{
		vv.iframe = document.createElement('iframe')
		vv.iframe.width = '200';  	// For example 200 px.
		vv.iframe.height = '200';		// For example 200 px.
		vv.iframe.src = 'about:blank'; 
	}
	//	Append iframe to parent.
	{
		vv.parentEl.appendChild(vv.iframe)
	}
	//	Inject content into iframe.
		vv.iframeDoc = vv.iframe.contentWindow.document
		vv.iframeCoreHTML = generateCoreHTML();
		vv.iframeDoc.write(vv.iframeCoreHTML)	

}



//	----------------------------
//	HELPER FUNCTIONS:

function generateCoreHTML() {
	var theCoreHTML ='<!DOCTYPE html>'
		+'<html>'
		+'	<head>'
		+'		<title></title>'
		+'		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'
		+'	</head>'
		+'	<body>'
		+'		BOOOO'
		+'	</body>'
		+'</html>';
	return theCoreHTML;
}


function loadJsOrCssFile(doc, filename, filetype){
	if (filetype === "js") { 		//If filename is a external JavaScript file.
		var fileref=doc.createElement("script");
		fileref.setAttribute("type","text/javascript");
		fileref.setAttribute("src", filename);
	}
	else if (filetype === "css") { 	//If filename is an external CSS file.
		var fileref=doc.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", filename);
	}
	if (typeof fileref !== "undefined") {
		doc.getElementsByTagName("head")[0].appendChild(fileref);
	}
}
