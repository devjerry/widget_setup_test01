//	INLINE ROUTINES:
{
	console.log(">>> innerBootstrapper.js -- hi.")

	//	Write out body content.
	{
		var strVar="";
		strVar += "	<!-- ALL CONTENT GOES IN HERE -->";
		strVar += "	<div data-ui-view='PageViewHolder' class='_viewHolder PageViewHolder'>";
		strVar += "	<\/div>";
		strVar += "";
		strVar += "	<!-- MESSAGE UNTIL ANGULAR RUNS -->";
		strVar += "	<div class='PleaseWaitMssg' style='display:{{vv.PleaseWaitMssgStyleDisplay}}'>";
		strVar += "		Page will load soon.";
		strVar += "	<\/div>";
		strVar += "";
		strVar += "	<!-- JAVASCRIPT - TO BE BUNDLED -->";
		strVar += "	<script src='\/\/code.jquery.com\/jquery-2.1.1.min.js'><\/script>";
		strVar += "	";
		strVar += "	<script src=\"\/\/code.angularjs.org\/1.3.0-beta.17\/angular.js\"><\/script>";
		strVar += "	<script src='\/\/code.angularjs.org\/1.3.0-beta.17\/angular-resource.js'><\/script>";
		strVar += "	";
		strVar += "	<script src='\/\/cdnjs.cloudflare.com\/ajax\/libs\/angular-ui-router\/0.2.10\/angular-ui-router.min.js'><\/script>";
		strVar += "	";
		strVar += "";
		strVar += "	<script src='0a-bundles\/MainBundle.js'><\/script>";
		
		document.write(strVar)
	}
	
	
	//	Content should be layed out on page, so run whenLoaded().
	{
		whenLoaded();
	}
}


//	HELPER FUNCTIONS LIBRARY:

	 function whenLoaded() {
		var vv = {}

		//	What we define as steps.
		{
			// Snap iframe height to content height.
			{
				vv.height = document.body.scrollHeight
				if (vv.height > 10) {
					vv.height = vv.height + 20
				}
				window.frameElement.style.height = vv.height + 'px'
			}
		}
		
		//	Do what client defines as steps.
		{
			var outer_config_name = window.frameElement.getAttribute('data-outer_config_name')
			parent.window[outer_config_name].whenLoadedFnc()
			///console.dir("outer_config ",  outer_config)
		}
	}
